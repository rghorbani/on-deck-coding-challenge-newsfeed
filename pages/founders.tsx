import { Fragment, useEffect, useState } from 'react';
import { useQuery, gql } from '@apollo/client';
import styled from 'styled-components';
import Head from 'next/head';
import ReactInfiniteScroll from 'react-infinite-scroller';

import AnnouncementCard from 'components/AnnouncementCard';
import Layout from 'components/Layout';
import UserRow from 'components/UserRow';
import { User } from 'types';

const FELLOWSHIP = 'founders';
const FEED_QUERY = gql`
  query feed($fellowship: String!, $page: Int) {
    feed(fellowship: $fellowship, page: $page) {
      data {
        id
        fellowship
        title
        body
      }
      info {
        page
        limit
        totalPages
      }
    }

    users(fellowship: $fellowship) {
      id
      name
      bio
      fellowship
      avatar_url
      projects {
        id
        name
        icon_url
      }
    }
  }
`;

type QueryData = {
  feed: {
    data: Announcement[];
    info: {
      page: number;
      limit: number;
      totalPages: number;
    };
  };
  users: User[];
};

type QueryVars = {
  fellowship: string;
  page: number;
};

type Announcement = {
  id: number;
  fellowship: string;
  title: string;
  body: string;
  created_ts: string;
};

export default function Founders(): JSX.Element {
  const [items, setItems] = useState<Announcement[]>([]);
  const [hasMore, setHasMore] = useState<boolean>(false);

  const { data, error, loading, fetchMore } = useQuery<QueryData, QueryVars>(FEED_QUERY, {
    // skip: !query.fellowship,
    variables: {
      fellowship: FELLOWSHIP,
      page: 1,
    },
  });

  useEffect(() => {
    setItems(data?.feed?.data || []);
    setHasMore(
      (data?.feed && data?.feed.info && data?.feed?.info.page < data?.feed?.info.totalPages) ||
        false,
    );
  }, [data]);

  const users: User[] = data?.users || [];

  return (
    <Layout>
      <Head>
        <title>Founders Newsfeed</title>
      </Head>
      <h1>Founders Newsfeed</h1>

      <UserRow users={users} />

      <ReactInfiniteScroll
        pageStart={1}
        hasMore={hasMore}
        loader={
          <div className="loader" key={0}>
            Loading ...
          </div>
        }
        loadMore={(page: number) => {
          fetchMore({
            variables: {
              fellowship: FELLOWSHIP,
              page,
            },
          }).then((fetchMoreResult: { data: QueryData }) => {
            setItems([...items, ...(fetchMoreResult.data.feed?.data || [])]);
            setHasMore(
              (fetchMoreResult.data.feed.info &&
                fetchMoreResult.data.feed.info.page < fetchMoreResult.data.feed.info.totalPages) ||
                false,
            );
          });
        }}
      >
        <div>
          {items.map((announcement: Announcement) => (
            <Fragment key={announcement.id}>
              <AnnouncementCard announcement={announcement} />
              <Space />
            </Fragment>
          ))}
        </div>
      </ReactInfiniteScroll>
    </Layout>
  );
}

const Space = styled.div`
  display: block;
  height: 1em;
`;
