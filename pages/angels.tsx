import FellowshipPage from 'components/FellowshipPage';

const FELLOWSHIP = 'angels';

export default function Angels(): JSX.Element {
  return <FellowshipPage fellowship={FELLOWSHIP} />;
}
