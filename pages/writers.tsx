import FellowshipPage from 'components/FellowshipPage';

const FELLOWSHIP = 'writers';

export default function Writers(): JSX.Element {
  return <FellowshipPage fellowship={FELLOWSHIP} />;
}
