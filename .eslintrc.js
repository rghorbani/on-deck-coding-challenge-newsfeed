const OFF = 0;
const WARNING = 1;
const ERROR = 2;

module.exports = {
	root: true,
	env: {
		"browser": true,
		"commonjs": true,
		"es6": true,
		"node": true
	},
	settings: {
		"react": {
			"version": "detect"
		},
		"import/resolver": {
			"typescript": {},
		},
	},
	parserOptions: {
		project: ['./tsconfig.json'],
	},
	plugins: ['@typescript-eslint', 'eslint-comments', 'prettier'],
	extends: [
    'airbnb',
		"airbnb-typescript",
		"airbnb/hooks",
		"plugin:@typescript-eslint/recommended",
		"plugin:eslint-comments/recommended",
		"prettier",
	],
	rules: {
    'prettier/prettier': ERROR,
    '@typescript-eslint/ban-types': WARNING,
    '@typescript-eslint/no-use-before-define': OFF,
    'import/export': OFF,
    'import/extensions': OFF,
    'import/order': OFF,
    'import/no-cycle': OFF,
    'import/no-duplicates': OFF,
    'import/no-extraneous-dependencies': OFF,
    'import/no-named-as-default': OFF,
    'import/no-named-as-default-member': OFF,
    'import/no-self-import': OFF,
    'import/no-useless-path-segments': OFF,
    'react/react-in-jsx-scope': OFF,
    'react/jsx-props-no-spreading': OFF,
    'no-underscore-dangle': WARNING,
	},
};
