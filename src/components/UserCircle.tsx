import Link from 'next/link';
import styled from 'styled-components';
import Card from './Card';
import ProjectRow from './ProjectRow';
import { User } from '../types';

type Props = {
  user: User;
};

export default function UserCircle({ user }: Props): JSX.Element {
  return (
    <Card>
      <Columns>
        <Link href={`/users/${user.id}`}>
          <AvatarContainer>
            <Avatar src={user.avatar_url} />
          </AvatarContainer>
        </Link>

        <Detail>
          <H2>{user.name}</H2>
          <Fellowship>{user.fellowship}</Fellowship>
          {!!user.projects.length && (
            <>
              <H3>Projects</H3>
              {user.projects.map(p => (
                <ProjectRow key={p.id} project={p} />
              ))}
            </>
          )}
        </Detail>
      </Columns>
    </Card>
  );
}

const Columns = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const H2 = styled.h2`
  margin-top: 0.5em;
  text-align: center;
`;

const Fellowship = styled.div`
  margin-top: -0.5em;
  text-align: center;
`;

const H3 = styled.h3`
  margin-top: 1em;
  text-align: center;
`;

const AvatarContainer = styled.div`
  display: flex;
  align-items: center;
`;

const Avatar = styled.img`
  background-color: rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  width: 10em;
  cursor: pointer;
`;

const Detail = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  flex-shrink: 0;
  flex-basis: 14rem;
`;
