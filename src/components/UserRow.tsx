import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
import styled from 'styled-components';

import UserCircle from './UserCircle';
import { User } from '../types';

type Props = {
  users: User[];
};

export default function UserRow({ users }: Props): JSX.Element {
  return (
    <Row>
      <Swiper
        slidesPerView={3}
        modules={[Navigation]}
        spaceBetween={10}
        navigation
        pagination={{ clickable: true }}
      >
        {users.map(user => (
          <SwiperSlide>
            <UserCircle key={user.id} user={user} />
          </SwiperSlide>
        ))}
      </Swiper>
    </Row>
  );
}

const Row = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 1em;
`;
