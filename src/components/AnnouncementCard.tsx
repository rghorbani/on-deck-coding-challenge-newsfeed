import styled from 'styled-components';
import Card from './Card';
import Markdown from './Markdown';
import { Announcement } from '../types';

type Props = {
  announcement: Announcement;
};

export default function AnnouncementCard({ announcement }: Props): JSX.Element {
  return (
    <Card>
      <Column>
        <h2>{announcement.title}</h2>
        <Markdown>{announcement.body}</Markdown>
      </Column>
    </Card>
  );
}

const Column = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 21rem;
`;
