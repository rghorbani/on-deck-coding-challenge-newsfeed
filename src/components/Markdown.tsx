import marked from 'marked';
import styled from 'styled-components';

type Props = {
  children: string;
};

export default function Markdown({ children }: Props): JSX.Element {
  const html = marked(children);

  return <MarkdownContainer dangerouslySetInnerHTML={{ __html: html }} />;
}

const MarkdownContainer = styled.div``;
