export type Announcement = {
  id: number;
  fellowship: string;
  title: string;
  body: string;
  created_ts: string;
};
