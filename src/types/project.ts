export type Project = {
  id: number;
  name: string;
  icon_url: string;
};
