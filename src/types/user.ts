import { Project } from './project';

export type User = {
  id: number;
  name: string;
  bio: string;
  fellowship: 'fellows' | 'angels' | 'writers';
  avatar_url: string;
  projects: Project[];
};
