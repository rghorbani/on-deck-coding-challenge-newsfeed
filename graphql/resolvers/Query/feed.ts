import db, {AnnouncementRow} from '../../db'

type Args = {
  fellowship: string;
  page?: number;
  limit?: number;
}

type Response = {
  data: AnnouncementRow[];
  info: {
    page: number;
    limit: number;
    totalPages: number;
  }
};

export default async function feed(parent: unknown, {fellowship, page, limit}: Args): Promise<Response> {
  if (limit === undefined) {
    limit = 2;
  }
  if (page === undefined) {
    page = 1;
  }

  const count: any = await db.count(
    'SELECT COUNT(*) AS count FROM announcements WHERE fellowship = ? OR fellowship = "all"',
    [fellowship],
  );

  const feed: AnnouncementRow[] = await db.getAll(
    'SELECT * FROM announcements WHERE fellowship = ? OR fellowship = "all" ORDER BY created_ts DESC LIMIT ? OFFSET ?',
    [fellowship, limit, (page - 1) * limit],
  );

  return {
    data: feed,
    info: {
      page,
      limit,
      totalPages: Math.ceil(count / limit),
    },
  };
}
