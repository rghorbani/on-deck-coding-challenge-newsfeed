import db, {UserRow} from '../../db'

type Args = {
  fellowship: string;
}

export default async function users(parent: unknown, {fellowship}: Args): Promise<UserRow[]> {
  let users: UserRow[];
  if (fellowship === 'founders' || fellowship === 'angels') {
    users = await db.getAll(
      'SELECT * FROM users WHERE fellowship = ? OR fellowship = ?',
      ['founders', 'angels'],
    );
  } else if (fellowship === 'writers') {
    users = await db.getAll(
      'SELECT * FROM users WHERE fellowship = ?',
      [fellowship],
    );
  } else {
    users = [];
  }
  return users;
}
