import {ApolloServer, gql} from 'apollo-server-micro'
import * as resolvers from './resolvers'

const typeDefs = gql`
  type Announcement {
    id: Int!
    fellowship: String!
    title: String!
    body: String!
  }

  type Project {
    id: Int!
    name: String!
    description: String!
    icon_url: String!
    users: [User!]!
  }

  type User {
    id: Int!
    name: String!
    bio: String!
    avatar_url: String!
    fellowship: String!
    projects: [Project!]!
  }

  type ResponseInfo {
    page: Int!
    limit: Int!
    totalPages: Int!
  }

  type FeedResponse {
    data: [Announcement!]!
    info: ResponseInfo
  }

  type Query {
    feed(fellowship: String!, page: Int, limit: Int): FeedResponse!
    project(id: Int!): Project!
    user(id: Int!): User!
    users(fellowship: String!): [User!]!
  }
`;

export const server = new ApolloServer({typeDefs, resolvers})
